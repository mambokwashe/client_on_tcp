Microchip ENC28J60 Ethernet Controlloer
=======================================

The ENC424J600 and ENC624J600 are stand-alone,
Fast Ethernet controllers with an industry standard
Serial Peripheral Interface (SPI) or a flexible parallel
interface. They are designed to serve as an Ethernet
network interface for any microcontroller equipped with
SPI or a standard parallel port.

ENC424J600/624J600 devices meet all of the
IEEE 802.3 specifications applicable to 10Base-T and
100Base-TX Ethernet, including many optional
clauses, such as auto-negotiation. They incorporate a
number of packet filtering schemes to limit incoming
packets. They also provide an internal, 16-bit wide
DMA for fast data throughput and support for hardware
IP checksum calculations.

For applications that require the security and authentication features of SSL,
TLS and other protocols related
to cryptography, a block of security engines is provided.
The engines perform RSA, Diffie-Hellman, AES, MD5
and SHA-1 algorithm computations, allowing reduced
code size, faster connection establishment and
throughput, and reduced firmware development effort.

Communication with the microcontroller is
implemented via the SPI or parallel interface, with data
rates ranging from 14 Mbit/s (SPI) to 160 Mbit/s
(demultiplexed, 16-bit parallel interface). Dedicated
pins are used for LED link and activity indication and for
transmit/receive/DMA interrupts.

A generous 24-Kbyte on-chip RAM buffer is available
for TX and RX operations. It may also be used by the
host microcontroller for general purpose storage.
Communication protocols, such as TCP, can use this
memory for saving data which may need to be
retransmitted.

For easy end product manufacturability, each
ENC624J600 family device is preprogrammed with a
unique nonvolatile MAC address. In most cases, this
allows the end device to avoid a serialized
programming step.

The only functional difference between the
ENC424J600 (44-pin) and ENC624J600 (64-pin)
devices are the number of parallel interface options
they support. 

Features
--------
General:
* IEEE 802.3 Compliant Fast Ethernet Controller
* Integrated MAC and 10/100Base-T PHY
* Hardware Security Acceleration Engines
* Factory Preprogrammed Unique MAC Address
* Supports one 10/100Base-T Port with Automatic Polarity Detection and Correction
* Supports Auto-Negotiation
* Support for Pause Control Frames, including Automatic Transmit and Receive Flow Control
* Supports Half and Full-Duplex Operation
* Programmable Automatic Retransmit on Collision Available MCU Interfaces:
* 14 Mbit/s SPI interface with enhanced set of opcodes
* 8-bit multiplexed parallel interface Security Engines:
* High-performance, modular exponentiation engine with up to 1024-bit operands
* Supports RSA and Diffie-Hellman key exchange algorithms
* Fast MD5 hash computations
* Fast SHA-1 hash computations.

Support
-------
[Home]: http://www.microchip.com/ENC624J600

Knows issues and workarounds
----------------------------
Currently The AVR SPI driver will not init the MOSI and SCK pin.
The Application and Example for TCPIP Lite Stack should modified after
import project into IDE(Atmel Stdio 7 or IAR).
The workaroud is put below code into driver_init.c->system_init() function:
MOSI_set_dir(PORT_DIR_OUT);
SCK_set_dir(PORT_DIR_OUT);
The MOSI and SCK keyword in above function maybe different with projects.
Please change them according the atmel_start_pins.h
