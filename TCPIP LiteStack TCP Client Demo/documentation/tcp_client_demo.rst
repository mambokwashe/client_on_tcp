TCPIP Lite Stack TCP Client Demo
================================

This is a TCP client implementation that will connect to
a server that runs on a computer on port 60. The user
needs to modify the server IP address into the
firmware. Once the connection is established, the client
will send status packets to the server every two
seconds. The packets sent by the client contain
temperature reading(not implemented, always be 25c),
potentiometer value(not implemented always be 3.3v, buttons and
LED status. From the computer/server the user can
send text messages that will be printed on the MCU board's USART. Messages
longer than 16 characters will be truncated and only the
first 16 characters will appear on the USART. From the
server, the user can also turn the LEDs on the board
ON or OFF using the GUI push buttons.

For this demo there is only one active connection
implemented, but the TCP/IP stack supports multiple
TCP connections on the same board. For each new
connection the user needs to create a new socket, an
RX buffer, and try to connect to the server. The firmware
was developed to run on MCU boards.

The TCP Client Demo will try to connect to the server
every two seconds. This was implemented so that the
user has easy access to the Wireshark protocol
analyzer. 

Setup the Software for TCP Client Demo
--------------------------------------
* Find the IP Address of the computer where the
  TCP/IP Server Java Demo application runs on.
* Open IDE and load the TCP Client Demo project.
* Modify the Server IP address from TCP/IP Stack
  Demo Code (e.g., remoteSocket.addr.s_addr =
  MAKE_IPV4_ADDRESS(192,168,0,3);).
* If the DHCP server is running in the Ethernet
  network used for the demo, the board should get its own IP address. The IP
  address will come up on the USART.
* Start the TCP/IP Demo Java application on the computer
  (unzip download atzip file and run TCPIP_Demo.jar). The application was build
  by java, please make sure JRE was installed in PC.
* Go to the TCP Server Demo tab.
* Go to the Server  Local Port and change the port number to 60.
* Push the Listen button.
* When the board connects to the computer, a message in the Sent/Received Data
  window will appear (e.g., 192.168.0.21: Connected).
* Type in the Send field and the message will be
  sent to the board. The message will be sent
  when either the Enter or the Send buttons are
  pushed. Both sent data and received data will be
  shown in different colors in the Received/Send
  Data window.
* Messages that came from the board will be
  automatically shown in the Sent/Received Data
  window and include information on raw
  temperature (not implemented), button 0 states,
  LED0 states and the raw value of
  the on-board potentiometer. Values are in
  hexadecimal format.
* Pushing the LED 0 buttons will initiate
  the sending of a command to the board. The
  LED0 from the boards will be
  turned ON or OFF. The implementation supports
  only one LED turning ON or OFF at a time.
* Pushing the Disconnect button will close the
  TCP connection. A "Client disconnected"
  message will appear.
* Repeat steps 8 to 14 to test the connection

Required Hardware and Software to Run the Demo
----------------------------------------------
* MCU demo Board
* ENC424J600/624J600 extenstion board (Ethernet4 XPlained Pro), the Ethernet4
  XPlained Pro board is used by demo's default configuration. Application can
  also use ENC28J60 extention board(Ethernet3 XPlained Pro), Just change the
  TCPIP_STACK_INTERFACE variant to ENC28J60-SPIIF in Start configuration.
* PC with Windows, Linux or Mac OS
* TCP/IP Demo Application
	* TCPIP_Demo.jar was compressed at downloaded example.atzip file
	* The application was build by java, please make sure JRE was installed in PC.
* DHCP Server (without it the board cannot release an IP address and the demo
  will not work)
* Ethernet cables:
	* Straight-Through: if the board is connected to a router/switch
	* Crossover: if the board is connected directly to the computer

Setting Up the Hardware
-----------------------
Connect the Ethernet4 XPRO board to the EXT1 connector of MCU XPRO board.
Connect the board using an Ethernet cable to an Ethernet
network (it can be connected directly to the Ethernet port of a PC).
The board has to be able to connect to a running DHCP Server.

Knows issues and workarounds
----------------------------
Currently The AVR SPI driver will not init the MOSI and SCK pin.
The Application and Example for TCPIP Lite Stack should modified after
import project into IDE(Atmel Stdio 7 or IAR).
The workaroud is put below code into driver_init.c->system_init() function:
MOSI_set_dir(PORT_DIR_OUT);
SCK_set_dir(PORT_DIR_OUT);
The MOSI and SCK keyword in above function maybe different with projects.
Please change them according the atmel_start_pins.h
