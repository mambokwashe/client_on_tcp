/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_init.h"
#include <peripheral_clk_config.h>
#include <utils.h>
#include <hal_init.h>
#include <hpl_gclk_base.h>
#include <hpl_pm_base.h>

struct spi_m_sync_descriptor COMMUNICATION_IO;
struct timer_descriptor      Timer;

struct usart_sync_descriptor EDBG_COM;

void COMMUNICATION_IO_PORT_init(void)
{

	// Set pin direction to input
	gpio_set_pin_direction(ENCX24J600_MISO, GPIO_DIRECTION_IN);

	gpio_set_pin_pull_mode(ENCX24J600_MISO,
	                       // <y> Pull configuration
	                       // <id> pad_pull_config
	                       // <GPIO_PULL_OFF"> Off
	                       // <GPIO_PULL_UP"> Pull-up
	                       // <GPIO_PULL_DOWN"> Pull-down
	                       GPIO_PULL_OFF);

	gpio_set_pin_function(ENCX24J600_MISO, PINMUX_PA04D_SERCOM0_PAD0);

	gpio_set_pin_level(ENCX24J600_MOSI,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(ENCX24J600_MOSI, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(ENCX24J600_MOSI, PINMUX_PA06D_SERCOM0_PAD2);

	gpio_set_pin_level(ENCX24J600_SCK,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(ENCX24J600_SCK, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(ENCX24J600_SCK, PINMUX_PA07D_SERCOM0_PAD3);
}

void COMMUNICATION_IO_CLOCK_init(void)
{
	_pm_enable_bus_clock(PM_BUS_APBC, SERCOM0);
	_gclk_enable_channel(SERCOM0_GCLK_ID_CORE, CONF_GCLK_SERCOM0_CORE_SRC);
}

void COMMUNICATION_IO_init(void)
{
	COMMUNICATION_IO_CLOCK_init();
	spi_m_sync_init(&COMMUNICATION_IO, SERCOM0);
	COMMUNICATION_IO_PORT_init();
}

void EDBG_COM_PORT_init(void)
{

	gpio_set_pin_function(EDBG_COM_TX, PINMUX_PA22C_SERCOM3_PAD0);

	gpio_set_pin_function(EDBG_COM_RX, PINMUX_PA23C_SERCOM3_PAD1);
}

void EDBG_COM_CLOCK_init(void)
{
	_pm_enable_bus_clock(PM_BUS_APBC, SERCOM3);
	_gclk_enable_channel(SERCOM3_GCLK_ID_CORE, CONF_GCLK_SERCOM3_CORE_SRC);
}

void EDBG_COM_init(void)
{
	EDBG_COM_CLOCK_init();
	usart_sync_init(&EDBG_COM, SERCOM3, (void *)NULL);
	EDBG_COM_PORT_init();
}

void delay_driver_init(void)
{
	delay_init(SysTick);
}

/**
 * \brief Timer initialization function
 *
 * Enables Timer peripheral, clocks and initializes Timer driver
 */
static void Timer_init(void)
{
	_pm_enable_bus_clock(PM_BUS_APBC, TC3);
	_gclk_enable_channel(TC3_GCLK_ID, CONF_GCLK_TC3_SRC);

	timer_init(&Timer, TC3, _tc_get_timer());
}

void system_init(void)
{
	init_mcu();

	// GPIO on PA05

	gpio_set_pin_level(NCS_PIN,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(NCS_PIN, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(NCS_PIN, GPIO_PIN_FUNCTION_OFF);

	// GPIO on PB30

	gpio_set_pin_level(LED0,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   true);

	// Set pin direction to output
	gpio_set_pin_direction(LED0, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(LED0, GPIO_PIN_FUNCTION_OFF);

	COMMUNICATION_IO_init();

	EDBG_COM_init();

	delay_driver_init();

	Timer_init();
}
